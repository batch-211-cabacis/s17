console.log("Hello World");
/*
1. Create a function which is able to prompt the user to provide their fullname, age, and location.
-use prompt() and store the returned value into a function scoped variable within the function.
-display the user's inputs in messages in the console.
-invoke the function to display your information in the console.
-follow the naming conventions for functions.
*/

//first function here:

function printWelcomeMessage(){
	let fullName = prompt("Enter your Full Name");
	let age = prompt("Enter your age");
	let location = prompt("Enter your location");
	console.log("You are " + fullName + " " + age + " ");
	console.log("You live in " + location);
};
printWelcomeMessage();

/*
2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
-invoke the function to display your information in the console.
-follow the naming conventions for functions.

*/

//second function here:

function faveBands(){
	let band1 = "1. The Script";
	console.log(band1);
	let band2 = "2. LANY";
	console.log(band2);
	let band3 = "3. Spice Girls";
	console.log(band3);
	let band4 = "4. Westlife";
	console.log(band4);
	let band5 = "5. NSYNC";
	console.log(band5);
}
faveBands();

/*
3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
-invoke the function to display your information in the console.
-follow the naming conventions for functions.

*/

//third function here:

function faveMovies(){
	let movie1 = "1. Eat, Pray, Love";
	console.log(movie1);
	let rating1 = "Rotten Tomato Rating: 100%"
	console.log(rating1);

	let movie2 = "2. The Notebook";
	console.log(movie2);
	let rating2 = "Rotten Tomato Rating: 98%"
	console.log(rating2);

	let movie3 = "3. A Walk to Remember";
	console.log(movie3);
	let rating3 = "Rotten Tomato Rating: 95%"
	console.log(rating3);

	let movie4 = "4. Moana";
	console.log(movie4);
	let rating4 = "Rotten Tomato Rating: 100%"
	console.log(rating4);

	let movie5 = "5. Pirates of the Carribean";
	console.log(movie5);
	let rating5 = "Rotten Tomato Rating: 90%"
	console.log(rating5);	
}
faveMovies();

/*
4. Debugging Practice - Debug the following codes and functions to avoid errors.
-check the variable names
-check the variable scope
-check function invocation/declaration
-comment out unusable codes.
*/

// // printUsers();
// 	let printFriends = function printUsers(){
// 	alert("Hi! Please add the names of your friends.");
// 	let friend1 = alert("Enter your first friend's name:"); 
// 	let friend2 = prom("Enter your second friend's name:"); 
// 	let friend3 = prompt("Enter your third friend's name:");

// 	console.log("You are friends with:")
// 	console.log(friend1); 
// 	console.log(friend2); 
// 	console.log(friend3); 
// };


// // console.log(friend1);
// // console.log(friend2);
// // console.log(friend3);

function printFriends(){
	console.log("You are freinds with:");
	let friend1 = prompt("Enter your friend's name 1");
	let friend2 = prompt("Enter your friend's name 2");
	let friend3 = prompt("Enter your friend's name 3");
	console.log(friend1);
	console.log(friend2);
	console.log(friend3);
};
printFriends();
