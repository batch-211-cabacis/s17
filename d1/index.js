console.log("Hello World");

// Functions
	// Functions in JavaScript are lines/blocks of codes that tell our device or application to perform a certain task when called/invoked
	// Functions are mostly created to create complicated tasks to run several lines of code in succession
	// They are also used to prevent repeating lines or blocks of codes that perform the same task/function

	// Function declarations
		// (function statement) defines a function with the specified parameters

		/*
			Syntax:

			function functionName() {
				code block (statement)
			}
		*/

		// function keyword - used to define a JavaScript function
		// functionName - the function name. Functions are names to be able to use later in the code
		// function block ({}) - the statement which comprise the body of the function. This is where the code is to be executed
		// we can assign a varuiable to hold a function, but that will be explained later

		function printName () {
			console.log("My name is John.");
		}
		// can be hoisted anywhere
		printName(); //invocation

		// semi colons are used to separate executable JavaScript statements.

		// Function Invocation
			// the code block and statements inside a function is not immediately executed when the function is defined. The code block and ststements inside a function is executed when the function is invoked or called.
			// it is common to use the term "call a function" instead of "invoke a function"

			printName();

			// declaredFunction(); //this results in error // much like variables, we cannot invoke a function we have yet to define

		// Function declarations vs Expressions
			// Function declarations
				// a function can be created through function declaration by using the function keyword and adding a function name
				// declared functions are not executed immediately. they are "saved for later use" and will be excuted later when they are invoke(call upon)

				declaredFunction(); //decalred functions can be hoisted as long as the function has been defiend

				function declaredFunction () {
					console.log("Hello World from declaredFunction()")
				}
				// Note: Hoisting is JavaScripts behavior for certain variables and functions to run or use them before their declaration

				declaredFunction();

			// Function expression 
				// a function can also be stored inside a variable. This is called function expression 
				// a function expression is an anonymous function assigned to the variableFunction

				// Anonymous function - function without a name

				// variableFunction(); // error

				/*
				error - function expressions, being stored in a let or const variable cannot be hoisted
				*/

				let variableFunction = function() {
					console.log("Hello Again");
				};

				variableFunction();

				// we can also create a function expression of a name function
				// however to invoke the function expression, we invoke it by its variable name, not by its function name
				// function expressions are always invoke (called) using the variable name

				let funcExpression = function funcName (){
					console.log("Hello from the Other Side");
				};

				// funcName(); //error bec its
				funcExpression();


				// you can reassign declared functions and function expressions to new anonymous functions

				declaredFunction = function() {
					console.log("updated declaredFunction");
				};

				declaredFunction();

				funcExpression = function() {
					console.log("updated funcExpression");
				};

				funcExpression ();

				// However we cannot reassign a function expression initialized with const

				const constantFunc = function() {
					console.log("Initialized with const!");
				};
				constantFunc();

				// constantFunc = function(){
				// 	console.log("Cannot be reassigned!");
				// };

				// constantFunc(); //will result to an error

			// Function Scoping
				/*
				Scope is the accesibility (visibility) or variables within our program
				JS variable has 3 types of scope
				1. local/block scope
				2. global scope
				3. function scope
				*/



				{
					let localVar = "Armando Perez";
					console.log(localVar);
				// only works or can be called within the curly braces
					// console.log(globalVar); // cannot be invoke global var inside a block code if it is not declared before our code block 
				} 

				let globalVar = "Mr Worldwide";
				console.log(globalVar); // cannot be called within the curly braces but can be called anywhere outside the curly braces
				// console.log(localVar); //will retrun error
				// localVar being in a block, cannot be accessed outside of its code block
				
			// Functions Scope
				/*
				JS has a function scope: Each functions creates a new scope
				Variables defined inside a function are not accessible (visible) from outside the function
				Variable declared with var, let and const are quite similar when declared inside a function
				*/

				function showNames() {
					// function scoped variables
					var functionVar = "Joe";
					const functionConst = "John";
					let functionLet = "Jane";

					console.log(functionVar);
					console.log(functionConst);
					console.log(functionLet);



				}; // whatever inside this is function scope

					showNames();

					// console.log(functionvar); //error
					// console.log(functionConst); //error
					// console.loh(functionLet); //error

					/*
					the variables functionVar, functionConst, functionLet  are function scoped and cannot be accessed outside of the function they were declared
					*/

				// Nested Functions
					// You can create another function inside a function
					// This is called a nested function

					function myNewFunction () {
						let name = "Cee";

						function nestedFunction () {
							let nestedName = "Thor";
							console.log(name);
							console.log(nestedName);
						}
						// console.log(nestedName);
						// results to an error since nestedName is not defined
						// nestedName variable, being declared in the nestedFunction cannot be accessed outside of the function it was declared in 
						nestedFunction();
					};
					myNewFunction();
					// nestedFunction();
					/*
						since this function is declared inside nyNewFunction it too cannot be invoked outside of the function it was declared in 
					*/

					// Function and Global Scoped Variables

					// Global scoped variable
					let globalName = "Nej";
					function myNewFunction2 (){
						let nameInside = "Martin";
						// variables declared globally (outside any function) have global scope
						// global variables can be accessed from anywhere in a JS program including from inside a function
						console.log(globalName);
					};

					myNewFunction2();

					// Using alert()
					// alert allows us to show a small window at the top of our browser page to show informatio to our users
					// as opposed to a consule.log() which will only show the message on the console
					// it allows us to show a short dialog or instruction to our user
					// the page will wait until the user dismisses the dialog

					alert("Hello World"); // this will run immediately when the page loads

					// alert () syntax
					// alert("<messageInString>");
					// you can do with numbers too

					// You can also use an alert() to show a message to the user from a later function invocation

					function showSampleAlert(){
						alert("Hello, user!")
					};
					showSampleAlert();
					// you will find that the page waits for the user to dismiss the dialog before proceeding
					// you can witness this by reloading the page while the console is open
					console.log("I will only log in the console when the alert is dismissed");

					// notes on the use of alert():
						// show only an alert() for short dialogs/messages to the user
						// do not overuse alert() because the program /js has to wait for it to be dismissed before continuing

					// Using prompt()
						// prompt() allows us to show a small window at the top of our browser to gather user input
						// it much like alert will have the page wait until the user completes or enters their input
						// the input from the prompt() will be returneed as a string once the user dismisses the window

						let samplePrompt = prompt('Enter your name')
						console.log("Hello, " + samplePrompt); 

						/*
						prompt() syntax:

						prompt("<dialogInString>");
						*/

						let sampleNullPrompt = prompt("Don't enter anything");
						console.log(sampleNullPrompt);
						// prompt() returns an empty string when there is no input and null when the user cancels the prompt

						// prompt() can be used for us to gather user input and be used in our code
						// however, since prompt() windows will hae the page wait until the user dismisses the window it must not be overused
						// prompt() use globally will be run immediately, so, for better user experience, it is much better to use them accordingly or add them insude a function

						function printWelcomeMessage(){
							let firstName = prompt("enter your First Name");
							let lastName = prompt("Enter your Last Name");
							console.log("Hello, " + firstName + " " + lastName + "!");
							console.log("Welcome to my page!");
						};
						printWelcomeMessage();

						// FUnction Naming Conventions
						// function name should be definitive of the task it will perform. It usually contains a verb

						// function getCourses (){
						// 	let courses = ["Science 101", "Math 101", "English 101"]
						// 	console.log(getCourses);
						// };
						// getCourses();

						// Avoid generic names to avoid confusion within your code

						function getName(){
							let name = "Jamie";
							console.log(name);
						}
						getName();

						// Avoid pointless and inappropriate function names

						// function remainder (){
						// 	console.log(25%5)
						// };
						// raminder();

						// Name your functions in small caps. Follow camelCase when naming variables and functions

						function displayCarInfo(){
							console.log("Brand: Toyota");
							console.log("Type: Sedan");
							console.log("Price: 1,500,000");
						};
						displayCarInfo();
